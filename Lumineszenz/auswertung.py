import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import curve_fit
from scipy import constants

files_laser = ["L{}.txt".format(l) for l in [805, 855, 900, 930, 960]]
data_laser = [np.loadtxt(f, skiprows=3) for f in files_laser]

files_background_off = ["H{}.txt".format(l) for l in [775, 825, 875, 925, 945]]
data_background_off = [np.loadtxt(f, skiprows=3) for f in files_background_off]
bg = np.mean(np.array(data_background_off)[:,:,1])

files_background_on = ["HLicht{}.txt".format(l) for l in [805, 855, 900, 930, 960]]
data_background_on = [np.loadtxt(f, skiprows=3) for f in files_background_on]

voltages = [1, 5, 10, 15, 20, 25, 30, 35, 40]
files_voltage = ["V{}.txt".format(v) for v in voltages]
data_voltage = [np.loadtxt(f, skiprows=3) for f in files_voltage]
for run in data_voltage:
    run[:,1] -= bg

powers = [0.56, 0.62, 1.58, 3.05, 4.79, 6.50, 8.32, 10.34, 12.10, 14.13, 16.00, 17.77, 19.74]
files_power = ["P{0:.2f}.txt".format(p) for p in powers]
data_power = [np.loadtxt(f, skiprows=3) for f in files_power]
data_power -= bg


def gaussian(x, a, b, c):
    return a * np.exp(-(x - b)**2 / c**2)

def gaussian_sum(x, *args):
    return gaussian(x, *args[:3]) + gaussian(x, *args[3:])

def gaussian_area(a, b, c):
    return np.sqrt(np.pi) * a * c

def join_spectrum(data):
    data_stacked = np.vstack(data)
    sort_indices = np.argsort(data_stacked[:,0])  # sort by wavelength
    return data_stacked[sort_indices,:]

def plot_spectrum(ax, data, color):
    for run in data:
        ax.plot(run[:,0], run[:,1], c=color)
    ax.set_xlabel("wavelength (nm)")
    ax.set_ylabel("intensity (a.u.)")
    plt.grid(True)

def plot_voltages(ax, data, voltages):
    for voltage, run in zip(voltages, data):
        ax.plot(np.ones(run[:,0].shape) * -voltage, run[:,0], run[:,1])
    ax.set_xlabel("voltage (mV)")
    ax.set_ylabel("wavelength (nm)")
    ax.set_zlabel("intensity (a.u.)")
    plt.grid(True)

def plot_voltages_flat(ax, data, voltages):
    for voltage, run in zip(voltages, data):
        ax.plot(run[:,0], run[:,1], label='-%d mV' % voltage)
    plt.grid(True)
    plt.xlabel("wavelength (nm)")
    plt.ylabel("intensity (a.u.)")
    plt.legend()

def plot_powers(ax, data, powers):
    for power, run in zip(powers, data):
        ax.plot(np.ones(run[:,0].shape) * power, run[:,0], run[:,1])
    ax.set_xlabel("power")
    ax.set_ylabel("wavelength (nm)")
    ax.set_zlabel("intensity (a.u.)")
    plt.grid(True)

def plot_powers_flat(ax, data, voltages):
    for power, run in zip(voltages, data):
        ax.plot(run[:,0], run[:,1], label='%.2f W' % power)
    plt.grid(True)
    plt.xlabel("wavelength (nm)")
    plt.ylabel("intensity (a.u.)")
    plt.legend()

def fit_gaussians_power(data):
    starts = [
        [1300, 824, 9, 1000, 842, 10],
        [1300, 824, 9, 1000, 842, 10],
        [4000, 824, 9, 2600, 842, 10],
        [6000, 824, 9, 4600, 842, 10],
        [8000, 824, 9, 6600, 842, 10],
        [12000, 824, 9, 7600, 842, 10],
        [16000, 824, 9, 9600, 842, 10],
        [20000, 824, 9, 13000, 842, 10],
        [23000, 824, 9, 15000, 842, 10],
        [27000, 824, 9, 19000, 842, 10],
        [30000, 824, 9, 24000, 842, 10],
        [35000, 824, 9, 29000, 842, 10],
        [39000, 824, 9, 34000, 842, 10],
    ]

    fits = []
    for start, run in zip(starts, data):
        popt, _ = curve_fit(gaussian_sum, run[:,0], run[:,1], start)
        fits.append(popt)
    #i = 0
    #x = data[i][:,0]
    #y = data[i][:,1]
    #plt.plot(x, y)
    #plt.plot(x, gaussian_sum(x, *starts[i]))
    #plt.plot(x, gaussian_sum(x, *fits[i]))
    #plt.show()
    #assert False
    return fits

def fit_gaussians_gate(data):
    from scipy.signal import savgol_filter
    starts = [
        [320, 928, 9, 350, 946, 10],
        [300, 928, 9, 350, 946, 10],
        [200, 928, 9, 350, 946, 10],
        [200, 928, 9, 350, 946, 10],
        [200, 928, 9, 350, 946, 10],
        [200, 922, 13, 280, 945, 9],
        [200, 922, 8, 300, 946, 10],
        [200, 922, 9, 280, 946, 10],
        [200, 922, 5, 280, 946, 10],
    ]

    """data = [
         row[0:600,:] for row in data
    ]"""
    smooth = lambda x: savgol_filter(x, 19, 1)
    #smooth = lambda x: x
    fits = []
    for start, run in zip(starts, data):
        popt, _ = curve_fit(gaussian_sum, run[:,0], smooth(run[:,1]), start)
        fits.append(popt)

    """if True:
        i = 6
        plt.clf()
        x = data[i][:,0]
        y = data[i][:,1]
        #plt.plot(x, y)
        plt.plot(x, smooth(y))
        plt.plot(x, gaussian_sum(x, *fits[i]))
        plt.plot(x, gaussian_sum(x, *starts[i]))
        plt.grid(True)
        plt.show()
        assert False"""
    return fits


def plot_power_areas(ax, powers, fits):
    s = [gaussian_area(*f[0:3]) + gaussian_area(*f[3:6]) for f in fits]
    ax.scatter(powers, s)
    ax.set_xlabel("power")
    ax.set_ylabel("area (a.u.)")
    plt.grid(True)

def plot_gate_areas(ax, voltages, fits):
    voltages_n = [-v for v in voltages]
    rechts = [gaussian_area(*f[3:6]) for f in fits]
    ax.scatter(voltages_n, rechts, label='peak rechts')
    links = [gaussian_area(*f[0:3]) for f in fits]
    ax.scatter(voltages_n, links, color='r', label='peak links')
    beide = [gaussian_area(*f[0:3]) + gaussian_area(*f[3:6]) for f in fits]
    ax.scatter(voltages_n, beide, label='beide peaks', color='g')
    ax.set_xlabel("voltage (mV)")
    ax.set_ylabel("area (a.u.)")
    plt.legend()
    plt.grid(True)
    return rechts

def gerade(x, a, b):
    return a*x + b

def tunnelbarriere(voltages, areas):
    voltages_n = [-v for v in voltages]
    popt, _ = curve_fit(gerade, voltages_n[2:], areas[2:])
    a, b = popt
    zero = -b / a
    x = np.linspace(-40, 10)
    plt.plot(x, gerade(x, *popt))

    V_g = zero

    d = (25 + 2 + 25 + 2*29 + 4) * 1e-9
    E_gap = 1.52 * constants.eV

    E_z = np.abs(V_g) / d - E_gap / (2 * constants.e * d)

    m = 0.07 * 9.1e-31
    tau = 1e-9
    L = 2e-9
    E = (constants.hbar * np.pi) ** 2 / (2 * m * L**2)

    tunnelbarriere = (
        np.log(
            tau / L * np.sqrt(E / 2 / m)
        )
        *
        (3 * constants.hbar * constants.e * E_z)
        /
        (4 * np.sqrt(2 * m))
    )**(2.0/3)

    print 'Tunnelbarriere = %.2f eV' % (tunnelbarriere / constants.eV)



fits_power = fit_gaussians_power(data_power)
fits_gate = fit_gaussians_gate(data_voltage)


if __name__ == "__main__":
    fig1 = plt.figure(1)
    ax1 = fig1.add_subplot(111)
    ax1.set_title("Lumineszenzspektrum")
    plot_spectrum(ax1, data_laser, "blue")
    plt.savefig('img/spektrum.png')

    fig2 = plt.figure(2)
    ax2 = fig2.add_subplot(111)
    ax2.set_title("Hintergrundspektrum abgedunkelt")
    plot_spectrum(ax2, data_background_off, "red")
    plt.savefig('img/bg-dunkel.png')

    fig3 = plt.figure(3)
    ax3 = fig3.add_subplot(111)
    ax3.set_title("Hintergrundspektrum nicht abgedunkelt")
    plot_spectrum(ax3, data_background_on, "green")
    plt.savefig('img/bg-hell.png')

    fig4 = plt.figure(4)
    ax4 = fig4.add_subplot(111, projection="3d")
    ax4.set_title("Spektren bei verschiedenen Gatterspannungen")
    plot_voltages(ax4, data_voltage, voltages)
    plt.savefig('img/gate.png')

    fig4 = plt.figure(8)
    plt.clf()
    ax4 = fig4.add_subplot(111)
    ax4.set_title("Spektren bei verschiedenen Gatterspannungen")
    filt = lambda d: np.array(d)[[0, 5, -1]]
    plot_voltages_flat(ax4, filt(data_voltage), filt(voltages))
    plt.savefig('img/gate-flat.png')

    fig5 = plt.figure(5)
    ax5 = fig5.add_subplot(111, projection="3d")
    ax5.set_title("Spektren bei verschiedenen Anregungsleistungen")
    plot_powers(ax5, data_power, powers)
    plt.savefig('img/power.png')

    fig5 = plt.figure(9)
    plt.clf()
    ax5 = fig5.add_subplot(111)
    ax5.set_title("Spektren bei verschiedenen Anregungsleistungen")
    plot_powers_flat(ax5, data_power, powers)
    plt.savefig('img/power-flat.png')

    fig6 = plt.figure(6)
    ax6 = fig6.add_subplot(111)
    ax6.set_title("Flaeche unter beiden Peaks")
    plot_power_areas(ax6, powers, fits_power)
    plt.savefig('img/peak_flaeche.png')

    fig7 = plt.figure(7)
    plt.clf()
    ax7 = fig7.add_subplot(111)
    areas = plot_gate_areas(ax7, voltages, fits_gate)

    tunnelbarriere(voltages, areas)

    plt.show()
