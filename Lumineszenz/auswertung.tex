\documentclass[12pt, a4paper, ngerman]{scrartcl}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{babel}
\usepackage{a4wide}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage{braket}
\usepackage{gensymb}
\usepackage{csvsimple}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{float}
\usepackage{hyperref}
\usepackage{breqn}
\usepackage{float}

\usepackage[a4paper]{geometry}

\sisetup{range-units=single, separate-uncertainty=true}


\title{Auswertung Praktikum K2}
\subtitle{Lumineszenz an Quantenpunkten}
\author{Jannik Luhn, Benjamin Wiegand}


\newcommand{\Spvek}[2][r]{%
	\gdef\@VORNE{1}
	\left(\hskip-\arraycolsep%
	\begin{array}{#1}\vekSp@lten{#2}\end{array}%
	\hskip-\arraycolsep\right)}

\def\vekSp@lten#1{\xvekSp@lten#1;vekL@stLine;}
\def\vekL@stLine{vekL@stLine}
\def\xvekSp@lten#1;{\def\temp{#1}%
	\ifx\temp\vekL@stLine
	\else
	\ifnum\@VORNE=1\gdef\@VORNE{0}
	\else\@arraycr\fi%
	#1%
	\expandafter\xvekSp@lten
	\fi}

\begin{document}
\maketitle


Im hier ausgewerteten Praktikumsversuch wurde eine Probe mit Quantenpunkten spektral untersucht.

\section{Theorie}
\subsection{Exzitonen}
In der Bandstruktur von Halbleitern finden sich sowohl ein beinahe vollbesetztes Valenz- sowie darüber ein kaum besetztes Leitungsband. Strahlt man Photonen mit Energien größer als die der Bandlücke ein, regt man Elektronen vom Valenz- in das Leitungsband an. Nach kurzer Zeit (etwa einigen ps) relaxieren diese durch Phononen- und Elektron-Elektron-Wechselwirkung auf die niedrigsten Energienivaus im Leitungsband. Dort können sie zusammen mit den im Valenzband zurückgelassenen Löchern ein gebundenes System bilden, welches als Exziton bezeichnet wird, und, ähnlich wie ein geladenes Teilchen in einem Coulombtopf, eine Bindungsenergie von

\begin{equation}
	E_{\text{Exziton}} = \frac{m^* e^4}{8 \epsilon^2 \epsilon_0^2 h^2}
\end{equation}

besitzt (n=1), wobei m* die effektive Masse, e die Elementarladung, \(\epsilon_0\) die elektrische Feldkonstante, h die Planck-Konstante und \(\epsilon\) eine Dielektrizitätskonstante darstellt. Im Banddiagramm befinden sich die Exzitonenniveaus knapp unterhalb des Leitungsbandes.
Beim Zerfall von Exzitonen werden, wie auch bei Interband-Übergängen, Photonen ausgesandt, welche als Lumineszenz beobachtet werden können.

\subsection{Quantenpunkte}
Mithilfe moderner Epitaxieverfahren ist es möglich, Halbleitersysteme aus Schichten verschiedener Materialien herzustellen, welche als Heterostrukturen bezeichnet werden. Ist eine Schicht geringer Bandlücke von zwei Schichten stärker ausgeprägter Bandlücke umgeben spricht man von einem Quantentopf. Durch Intraband-Relaxationen fallen in seiner Umgebung angeregte Exzitonen schnell in ihn hinein bevor sie dort lokalisiert zerfallen. Ist der Quantentopf sehr schmal, die mittlere Schicht also sehr dünn (vergleichbar mit der Größenordnung der de-Broglie-Wellenlänge), treten quantenmechanische Effekte auf: Die möglichen Zustände sind quantisiert, man spricht von z-Quantisierung.
Ist der Quantentopf nun auch noch in den beiden anderen räumlichen Achsen eingeschränkt spricht man von Quantenpunkten. Solche Strukturen können bei geeigneten Ober- und Grenzflächenenthalpien der beteiligten Materialien entstehen, welche stark von den jeweiligen Gitterkonstanten abhängen. Sie sind in der Regel wenige Monolagen dick und ungleichmäßig auf der Oberfläche verteilt.

\subsection{Energieniveaus in Quantenpunkten und Übergänge}
Um die Energieniveaus in Quantenpunkten zu berechnen, werden folgende Vereinfachungen angenommen:
\begin{itemize}
	\item Betrachtung der Quantenpunkte als zweidimensional
	\item Annahme eines  parabolischen Potentialverlaufs in xy-Richtung
	\item Vernachlässigung von Dot-Dot-Wechselwirkungen
\end{itemize}
Unter Verwendung der Gleichungen des harmonischen Oszillators erhält man:
\begin{equation}
	H = \frac{p_x^2 + m^{*2}\omega^2 q_x^2}{2m^*} + \frac{p_y^2 + m^{*2} \omega^2 q_y^2}{2m^*}.
\end{equation}
Löst man dieses Problem, so ergeben sich Eigenzustände \(\psi_{nm}\) mit \(n=0,1,2,...\) und \(m=-n,-n+2,...,n\).
Hierbei stößt man auf Auswahlregeln, die nur solche Übergänge, die die Hauptquantenzahl n nicht verändern; es gibt also nur s-s, p-p, ... -Übergänge.

\newpage
\section{Durchführung}
\subsection{Untersuchte Probe}
Im Versuch wurde eine GaAs-InGaAs-GaAs-Probe verwendet, wobei sich die Quantenpunkte in der dünnen InGaAs-Schicht befanden. Dadurch ergibt sich der in Abbildung \ref{fig:bandkantenverlauf} dargestellte Banddkantenverlauf. Durch Anlegen einer Gatespannung lässt sich dieser verformen, sodass sich die Energien der Quantenpunkte verschieben, was eine definierte Befüllung erlaubt.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/bandkantenverlauf.png}
	\caption{Bandkantenverlauf der verwendeten Probe. Quelle: Versuchsanleitung}
	\label{fig:bandkantenverlauf}
\end{figure}

\subsection{Versuchsaufbau}
Die Probe befindet sich auf einem mit flüssigem Helium gekühlten Probenhalter. Auf sie ist eine Glasfaser gerichtet, durch die sowohl anregendes Licht zur Probe hin, als auch emittiertes Licht von der Probe weggeführt wird. Diese beiden Lichtpfade werden von einem 50/50-Strahlteiler vor dem anderen Ende der Faser voneinander getrennt: In Reflexion befindet sich der bei 685nm operierender Laser, in Transmission ein Spektrometer zur Analyse der Lumineszenz sowie eine CCD-Kamera mit angeschlossener Auswerteelektronik.

In der Vorbereitung des Versuches wurde die Probe auf \(\SI{4.2}{\kelvin}\) heruntergekühlt.

\subsection{Kalibrierung}
Unter Verwendung des Maximums des Neonspektrums bei \(\SI{837.76}{\nano\metre}\) wurde vor Beginn der Messungen das Auswerteprogramm kalibriert.

\subsection{Durchführung}
Es wurde zuerst das Spektrum von \(\SI{775}{\nano\meter}\) bis \(\SI{975}{\nano\metre}\) mit maximaler Laserleistung aufgenommen. Anschließend wurde ein Hintergrundspektrum ohne Probe sowohl bei abgedunkeltem Raum als auch bei erhelltem Raum aufgenommen; hierbei ergaben sich durch die Deckenbeleuchtung deutlich sichtbare Peaks. Im nächsten Teilversuch wurden bei einer Laserleistung von etwa \(\SI{10}{\percent}\)  das Spektrum von \(\SI{920}{\nano\metre}\) bis \(\SI{970}{\nano\metre}\) bei verschiedenen Gatespannungen aufgenommen: \(V_G \in \{-5, -10, -15, -20, -25, -30, -35, -40\}\SI{}{\volt}\). Anschließend wurde die Abhängigkeit der Photolumineszenz von der eingestrahlen Leistung untersucht: Bei \(V_G=\SI{0}{\volt}\) wurde die Laserleistung zwischen \(\SI{0.56}{\watt}\) und \(\SI{19.74}{\watt}\) variiert.

\newpage
\section{Auswertung}

\subsection{Analyse des Spektrums}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/spektrum.png}
	\caption{}
	\label{fig:spektrum}
\end{figure}

\subsection{Analyse des Hintergrundspektrums}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/bg-hell.png}
	\caption{Das Hintergrundspektrum bei nicht abgedunkeltem Raum.}
	\label{fig:spektrum}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/bg-dunkel.png}
	\caption{Das Hintergrundspektrum bei abgedunkeltem Raum.}
	\label{fig:spektrum}
\end{figure}

\subsection{Variation der Gatespannung}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/gate.png}
	\caption{.}
	\label{fig:spektrum}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/gate-flat.png}
	\caption{.}
	\label{fig:spektrum}
\end{figure}

\subsection{Variation der Laserleistung}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/power.png}
	\caption{.}
	\label{fig:spektrum}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/power-flat.png}
	\caption{.}
	\label{fig:spektrum}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/peak_flaeche.png}
	\caption{.}
	\label{fig:spektrum}
\end{figure}


\end{document}
