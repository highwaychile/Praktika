# coding: utf-8
from matplotlib import pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from scipy import constants

KORR = False
KORR_F = '_korr' if KORR else ''
KORR_T = ' (korrigiertes Magnetfeld)' if KORR else ''

plt.figure(1)
plt.clf()
plt.figure(2)
plt.clf()
plt.figure(3)
plt.clf()
plt.figure(4)
plt.clf()
plt.figure(5)
plt.clf()
plt.figure(6)
plt.clf()
plt.figure(7)
plt.clf()
plt.figure(8)
plt.clf()
plt.figure(9)
plt.clf()

# Kontakte
US1US2 = 0
US1US4 = 1
S4S3 = 2
S1S4 = 3

quer = [US1US2, S4S3] # Kontakte, die Querspannung messen

amp = 100
I0 = 1.985e-6 # Strom durch Probe
deltaI0 = 0.015e-6 # Ungenauigkeit

fit_limit = 1.5 # Tesla

# Farben für einzelne Gatespannungen
colors = {
    -0.15:  'r',
    0:      'b',
    0.5:    'g',
}


configs = {
    #           Gate [V], x [mV/cm], y[V/cm], Kontakte
    'us-blau': [-0.15, 2, 0.2, US1US2],
    'us-rot': [-0.15, 2, 0.01, US1US4],
    'us-schwarz': [0, 2, 0.1, US1US2],
    'us-lila': [0, 2, 5e-3, US1US4],
    'us-pink': [0.5, 2, 5e-3, US1US4],
    'us-gruen': [0.5, 2, 0.1, US1US2],

    's-schwarz': [-0.15, 2, 0.2, S4S3],
    's-gruen': [-0.15, 2, 0.02, S1S4],
    's-lila': [0, 2, 5e-3, S1S4],
    's-orange': [0, 2, 0.2, S4S3],
    's-blau': [0.5, 2, 5e-3, S1S4],
    's-pink': [0.5, 2, 0.1, S4S3],
}
configs_detail = {
    'detail-schwarz': [0, 0.5, 2e-3, S1S4],
    'detail-blau':  [0.5, 0.5, 2e-3, S1S4],
    'detail-gruen': [-0.15, 0.5, 5e-3, S1S4]
}

# welche
plateau_rang = {
    'us-blau':  1,
    'us-schwarz': 2,
    'us-gruen': 2,

    's-pink': 2,
    's-schwarz': 2,
    's-orange': 2
}

# die y-Positionen der Plateaus
plateaus = {
    'us-blau': [133, 68, 46, 35],
    'us-schwarz': [137, 93, 69, 56, 47, 40],
    'us-gruen': [136, 92, 69, 56, 46, 39],

    's-pink': [135, 92, 69, 56, 47, 40],
    's-schwarz': [68, 46, 35, 27],
    's-orange': [69, 47, 35, 28, 24]
}

# die x-Positionen der Plateaus = B Magnetfeld
plateau_pos = {
    'us-blau':  [270, 155, 105, 78],
    'us-gruen': [250, 172, 130, 105, 86, 75],
    'us-schwarz': [212, 141, 105, 85, 72, 62],

    's-pink': [250, 167, 126, 101, 87, 75],
    's-schwarz': [162, 108, 82, 64],
    's-orange': [250, 137, 103, 83, 68]
}

# abwechselnd die Minima / Maxima der Längsspannung, die erste Zahl gibt an welche Ordnung das erst Minimum / Maximum wohl hat
minmax = {
    'us-rot': [1.5, [225, 151, 124, 101, 87, 76, 68, 61, 56, 51, 47]],
    'us-lila': [1.5, [249, 212, 161, 141, 120, 106, 94, 85, 77, 71, 65, 60, 57, 53]],
    'us-pink': [2, [259, 193, 173, 145, 131, 115, 105, 95, 88, 81, 75, 70, 65, 62, 57, 55, 52, 50, 48, 46, 44]],

    's-blau': [2, [247, 193, 168, 142, 126, 111, 102, 92, 84, 78, 73, 68, 64, 60, 57, 53, 51]],
    's-gruen': [1.5, [240, 158, 133, 107, 93, 80, 72, 64, 59]],
    's-lila': [2, [208, 173, 138, 118, 103, 92, 83, 75, 68, 63, 59, 54, 51, 47, 45, 43, 41]]
}

# bei der Längsspannung der Startwert
U0 = {
    'us-pink': 5,
    'us-lila': 15,
    'us-rot': 22,

    's-gruen': 27,
    's-blau': 37,
    's-lila': 64
}

laengs_detail = {
    'detail-gruen': [10, 45],
    'detail-schwarz': [18, 53],
    'detail-blau':  [8, 18, 26, 43, 58]
}

NEStrukturiert = {
    -0.15: [],
    0: [],
    0.5: []
}
DNEStrukturiert = {
    -0.15: [],
    0: [],
    0.5: []
}

# Initialisiere Dateien
ne_f = file('ne.tex', 'w')
ne_f.write("""\\begin{table}[H]
\\begin{tabular}{ll}
\\textbf{Bezeichner} & \\textbf{Ladungsträgerdichte}\\\\""")
plateau_f = file('plateau.tex', 'w')
minmax_f = file('sdh.tex', 'w')
ne_fuell = file('ne_fuell.tex', 'w')
beweglichkeit_f = file('beweglichkeit.tex', 'w')
beweglichkeit_f.write("""\\begin{table}[H]
\\begin{tabular}{lll}
\\textbf{Bezeichner} & \\textbf{Spannung [V]} & \\textbf{Beweglichkeit [\\(\\SI{}{\\metre^2\\volt^{-1}\\second^{-1}}\\)]}\\\\""")
crit_magn_f = file('crit_magn.tex', 'w')
crit_magn_f.write('\\begin{itemize}\n')
detail_max_f = file('detail_max.tex', 'w')
detail_max_f.write("""
\\begin{itemize}
""")
radius_f = file('radius.tex', 'w')
radius_f.write("""
\\begin{itemize}
""")

def ursprungsgerade(x, a):
    return a * x

def scaleX(x, xscale, ignoreKorr=False):
    """x [mm] ==> B [T]"""
    Ub = xscale * float(x) / 10 # einheit mV

    sc1 = 2.0049 # mv/A

    Ib = Ub / sc1 # einheit A

    sc2 = 0.226 # T / A

    B = Ib * sc2

    if KORR and not ignoreKorr:
        B += 0.25

    return B


def scaleY(y, yscale):
    """y [mm] ==> U[V]"""
    return yscale * float(y) / 10 / amp # einheit V

def plotstyle():
    plt.xlabel('Magnetfeld [T]')
    plt.ylabel('Gemessene Spannung [V]')
    plt.grid(True)
    plt.legend(loc='best')

def extract_data(kontakte, f):
    # wenn Hallspannung: Nullpunkt hinzufügen
    X = np.array([0] if kontakte in quer else [])
    Y = np.array([0] if kontakte in quer else [])

    # daten skalieren und in X und Y speichern
    for i, l in enumerate(f.readlines()):
        if i < 6: # ersten 6 Zeilen sind Mist
            continue

        x, y, _ = l.split(',') # einheit millimeter
        B = scaleX(x, xscale) # einheit Tesla
        y = scaleY(y, yscale) # einheit Volt

        X = np.append(X, [B])
        Y = np.append(Y, [y])
    return X, Y

def ne_mit_fitgerade(identifier, X, Y):
    XFit = X[X<1.5]
    YFit = Y[X<1.5]
    opt, cov = curve_fit(ursprungsgerade, XFit, YFit)

    steigung = opt[0]
    delta_steigung = np.sqrt(np.diag(cov))[0]

    ne = I0 / constants.e / (steigung)
    # Fehlerberechnung
    DneDsteigung = I0 / constants.e / (steigung**2)
    DneDI = 1 / constants.e / steigung
    deltane = DneDsteigung * delta_steigung + DneDI * deltaI0 #fehler von ne
    ne_f.write("\\textbf{%s}	&  \\(\\SI{%.2e}{\\per\\metre^3}\\pm \\SI{%.0e}{\\per\\metre^3}\\)\\\\\n" % (identifier, ne, deltane))

    return ne, deltane

def plot_fuellfaktor_kurven(us, ne, deltane, color, linestyle, label):
    # füllfaktor
    fuell = ne * constants.h / (2 * constants.e * np.linspace(0,6,100))
    delta_fuell = deltane * constants.h / (2 * constants.e * np.linspace(0,6,100))

    # plot füllfaktor
    plt.figure(us and 4 or 8)
    plt.plot(np.linspace(0,6,100), fuell, color + linestyle, label=label)
    plt.grid(True)
    plt.ylim(0, 10)
    plotstyle()
    plt.ylabel(u'Füllfaktor')

CONST = []

def plot_fuellfaktor_punkte(name, xscale, yscale):
    if name in plateau_pos:
        positions, ff, posY, deltapos = [], [], [], []

        for i, _ in enumerate(plateaus[name]):
            positions.append(scaleX(plateau_pos[name][i], xscale))
            deltapos.append(scaleX(5, xscale, True))
            posY.append(scaleY(plateaus[name][i], yscale))
            ff.append(i + plateau_rang[name])


        plt.errorbar(positions, ff, xerr=deltapos, fmt=pointstyle + color)

        plateau_f.write("""
\\item \\textbf{%s}
\\begin{table}[H]
\\begin{tabular}{%s}
\\textbf{B [T]}	&      %s  \\\\
\\textbf{\\(U_H\\) [V]} &  %s  \\\\
\\textbf{\\(\\nu\\)}	&  %s  \\\\
\\end{tabular}
\\end{table}
        """ % (
            identifier,
            'l' * (len(positions)+1),
            ' & '.join(('\\SI{%.2f}{}' % _) for _ in positions),
            ' & '.join(('\\SI{%.2e}{}' % _) for _ in posY),
            ' & '.join((str(_)) for _ in ff)
        ))

        for hallsp, fuellfaktor in zip(posY, ff):
            CONST.append(fuellfaktor * hallsp / I0 * 2)


def ne_mit_minmax(identifier, name, xscale, yscale):
    plt.figure(5)

    # berechne Ladungsträgerdichte mittels Minima / Maxima der Längsspannung
    start, positions = minmax[name]
    # Magnetfeld
    positions = [scaleX(pos, xscale) for pos in positions]

    Brez, fuellfaktoren, magn = [], [], []

    for i, B in enumerate(positions):
        fuellfaktor = start + (i * 0.5)
        Brez.append(1/B)
        fuellfaktoren.append(fuellfaktor)
        magn.append(B)

    # plot Füllfaktor vs. 1/B
    plt.plot(Brez, fuellfaktoren, pointstyle+color, label='U_g=%sV' % str(gate))
    plt.xlabel('1 / Magnetfeld [1/T]')
    plt.ylabel(u'Füllfaktor')
    plt.xlim(0, 1)
    plt.ylim(0, 10)
    plt.legend(loc='best')
    plt.grid(True)

    # Berechne Ladungsträgerdichte auf zweite Methode = ne2
    # berechne viele Einzelwerte, dann Mittelwerte
    ne2 = []
    deltane = []

    for i in xrange(len(positions)-2):
        # einzelwert für ne
        B_i = positions[i]
        B_ip2 = positions[i+2]
        ne_ = constants.e / constants.h / ((1.0/B_ip2) - (1.0/B_i))
        deltane_ = constants.e / constants.h / (((1.0/B_ip2) - (1.0/B_i))**2) * scaleX(3, xscale)
        deltane.append(deltane_)
        ne2.append(ne_*2)

    ne2 = np.mean(ne2)
    deltane = np.mean(deltane) / len(deltane)

    ne_fuell.write("%s	& \\(\\SI{%.1e}{} \\pm \\SI{%.0e}{}\\) \\\\" % (
        identifier, ne2, deltane
    ))

    minmax_f.write("""
\\item \\textbf{%s}
\\begin{table}[H]
\\begin{tabular}{%s}
\\textbf{B [T]}	&      %s  \\\\
\\textbf{\\(\\nu\\)}	&  %s  \\\\
\\end{tabular}
\\end{table}
    """ % (
        identifier,
        'l' * (len(magn)+1),
        ' & '.join(('\\SI{%.2f}{}' % _) for _ in magn),
        ' & '.join((str(_)) for _ in fuellfaktoren)
    ))
    return ne2, deltane


for name in sorted(configs.keys(), key=lambda n: configs[n][0]):
    us = name.startswith('us') # unstrukturiert oder strukturiert?
    f = file('%s.csv' % name, 'r') # daten

    [gate, xscale, yscale, kontakte] = configs[name]
    identifier = '%s%s' % (gate, 'US' if us else 'S') # name, z.B. -0.15S für V_g=-0.15V m strukturierten Bereich
    label = None if us else 'U_g=%sV' % str(gate) # label für plots
    color = colors[gate]
    linestyle = '--' if us else '-'
    pointstyle = ('.' if us else '*')

    X, Y = extract_data(kontakte, f)

    # messung der quer = Hallspannung
    if kontakte in quer:
        plt.figure(1)
        plt.xlim(0, 6)
        plt.plot(X, Y, color + linestyle, label=label)
        plotstyle()

        # fitgerade zur Bestimmung der Ladungsträgerdichte
        ne, deltane = ne_mit_fitgerade(identifier, X, Y)

        plot_fuellfaktor_kurven(us, ne, deltane, color, linestyle, label)
        plot_fuellfaktor_punkte(name, xscale, yscale)
    else:
        plt.figure(2)
        plt.xlim(0, 6)

        plt.plot(X, Y, color + linestyle, label=label)
        plotstyle()

        if name in minmax:
            ne, deltane = ne_mit_minmax(identifier, name, xscale, yscale)

    # Vergleich der erhaltenen Ladungsträgerdichten
    plt.figure(us and 6 or 9)
    plt.grid(True)
    plt.xlabel('Gatespannung [V]')
    plt.ylabel(u'Ladungsträgerdichte [1/m^3]')
    farbe = kontakte in quer and 'r' or 'b'

    if not name.startswith('detail'):
        # plot ladungsträgerdichten
        if kontakte in quer:
            plt.plot([gate], [ne], '.' + farbe, ms=10)
            plt.errorbar([gate], [ne], [deltane], color=farbe)
            if not us:
                NEStrukturiert[gate].append(ne)
                DNEStrukturiert[gate].append(deltane)
        else:
            plt.plot([gate], [ne], '*' + farbe, ms=10)
            plt.errorbar([gate], [ne], [deltane], color=farbe)
            if not us:
                NEStrukturiert[gate].append(ne)
                DNEStrukturiert[gate].append(deltane)

        if not us:
            # kritisches Magnetfeld für Runaway-Trajektorien berechnen
            if kontakte in quer:
                Bcrit = constants.hbar / constants.e * np.sqrt(2 * np.pi * ne) / (500e-9*0.5)
                crit_magn_f.write('\\item \\(B_{\\text{crit}}^{\\SI{%s}{\\volt}} = \\SI{%.2e}{\\tesla}\\)\n' % (
                    gate,
                    Bcrit
                ))

    farbe = us and 'r' or 'b'

    # beweglichkeit berechnen
    if name in U0:
        U = scaleY(U0[name], yscale)
        mu = 0.75 * I0 / U / constants.e / ne
        deltamu = deltaI0 / U * 0.75 / constants.e / ne + 0.75 * I0 / U / constants.e / (ne**2) * deltane
        beweglichkeit_f.write('%s & \\(\\SI{%.2e}{}\\) & \\(\\SI{%.1e}{} \\pm \\SI{%.0e}{}\\)\\\\\n' % (
            identifier,
            U0[name],
            mu,
            deltamu
        ))
        plt.figure(7)
        plt.grid(True)
        plt.plot([gate], [mu], '.' + farbe)
        plt.errorbar([gate], [mu], [deltamu], color=farbe)
        plt.xlabel('Gatespannung [V]')
        plt.ylabel('Beweglichkeit [m^2 V^-1 s^-1]')



for name in sorted(configs_detail.keys(), key=lambda n: configs_detail[n][0]):
    f = file('%s.csv' % name, 'r') # daten

    [gate, xscale, yscale, kontakte] = configs_detail[name]
    label = 'U_g=%sV' % str(gate)
    color = colors[gate]
    X, Y = extract_data(kontakte, f)

    plt.figure(3)
    plt.xlim(0, .9)

    plt.plot(X, Y, color, label=label)
    plotstyle()

    maxima = laengs_detail[name]

    ne = np.mean(NEStrukturiert[gate])
    dne = np.mean(DNEStrukturiert[gate]) / len(DNEStrukturiert[gate])

    detail_max_f.write("""\\item \\textbf{%s V}: %s""" % (
        gate,
        ', '.join(
            '\\SI{%.1e}{\\tesla}' % scaleX(maximum, xscale) for maximum in maxima
        )
    ))

    radii = []

    for maximum in maxima:
        vorfaktor = 2 * constants.hbar / constants.e / scaleX(maximum, xscale) * np.sqrt(2*np.pi)
        durchmesser = vorfaktor * np.sqrt(ne)
        Ddurchmesser = vorfaktor / 2 / np.sqrt(ne) * dne
        radii.append(durchmesser)
        print gate, '\\(%.2e \\pm %.0e\\)' % (durchmesser, Ddurchmesser)

    radius_f.write("""\\item \\textbf{%s V}: %s""" % (
        gate,
        ', '.join(
            '\\SI{%.1e}{\\metre}' % r for r in sorted(radii)
        )
    ))


print np.mean(CONST)
print np.sqrt(np.var(CONST))

plt.figure(1)
plt.title('Querspannung')
plt.savefig('quer.png')
plt.figure(2)
plt.title(u'Längsspannung')
plt.savefig('laengs.png')
plt.figure(3)
plt.title(u'Längsspannung Detail')
plt.savefig('laengs-detail.png')
plt.figure(4)
plt.title(u'Füllfaktor unstrukturiert' + KORR_T)
#plt.savefig('fuellfaktor_us%s.png' % KORR_F)
plt.figure(8)
plt.title(u'Füllfaktor strukturiert' + KORR_T)
#plt.savefig('fuellfaktor_s%s.png' % KORR_F)
plt.figure(5)
plt.title(u'Ermittlung von n_e')
plt.savefig('n_e.png')
plt.figure(6)
plt.title(u'n_e im US-Bereich' + KORR_T)
plt.savefig('vergleich_ne_us%s.png' % KORR_F)
plt.figure(9)
plt.title(u'n_e im S-Bereich' + KORR_T)
plt.savefig('vergleich_ne_s%s.png' % KORR_F)
plt.figure(7)
plt.title(u'Beweglichkeit')
plt.savefig('beweglichkeit.png')


plateau_f.close()
minmax_f.close()
ne_fuell.close()
ne_f.write("""\\end{tabular}
\\end{table}""")
ne_f.close()
beweglichkeit_f.write("""\\end{tabular}
\\end{table}""")
beweglichkeit_f.close()
crit_magn_f.write('\\end{itemize}')
crit_magn_f.close()
detail_max_f.write('\\end{itemize}')
detail_max_f.close()
radius_f.write('\\end{itemize}')
radius_f.close()