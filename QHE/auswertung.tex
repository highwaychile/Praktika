\documentclass[12pt, a4paper, ngerman]{scrartcl}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{babel}
\usepackage{a4wide}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage{braket}
\usepackage{gensymb}
\usepackage{csvsimple}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{float}
\usepackage{hyperref}
\usepackage{breqn}
\usepackage{float}

\usepackage[a4paper]{geometry}

\sisetup{range-units=single, separate-uncertainty=true}


\title{Auswertung Praktikum K1}
\subtitle{Quanten-Hall-Effekt}
\author{Jannik Luhn, Benjamin Wiegand}


\newcommand{\Spvek}[2][r]{%
	\gdef\@VORNE{1}
	\left(\hskip-\arraycolsep%
	\begin{array}{#1}\vekSp@lten{#2}\end{array}%
	\hskip-\arraycolsep\right)}

\def\vekSp@lten#1{\xvekSp@lten#1;vekL@stLine;}
\def\vekL@stLine{vekL@stLine}
\def\xvekSp@lten#1;{\def\temp{#1}%
	\ifx\temp\vekL@stLine
	\else
	\ifnum\@VORNE=1\gdef\@VORNE{0}
	\else\@arraycr\fi%
	#1%
	\expandafter\xvekSp@lten
	\fi}

\begin{document}
\maketitle


\section{Theoretische Grundlagen}
\subsection{Klassischer Hall-Effekt}
Fließt ein Strom \(I\) durch einen elektrischen Leiter, der senkrecht von einem Magnetfeld durchdrungen ist, so wirkt auf die Ladungsträger die Lorentzkraft, die schließlich zu einer Querspannung -- der Hall-Spannung -- führt.

Betrachtet man einen freien Ladungsträger (Ladung \(e\), Masse \(m\)), der sich sowohl in einem elektrischen Feld als auch in einem darauf senkrecht stehendem Magnetfeld \(B\) befindet, so bewegt dieser sich auf sogenannten Zyklotronbahnen, wobei die Zyklotronfrequenz
\[
\omega_c = \frac{e B}{m}
\]
beträgt. Mit Hilfe des Drude-Modells lässt sich die Bewegungsgleichung in Form des ohmschen Gesetzes schreiben:
\begin{equation}
\label{ohm}
	\begin{pmatrix}E_x\\E_y\end{pmatrix} = \sigma_0^{-1} \begin{pmatrix}1\quad -\omega_c \tau\\\omega_c \tau\quad 1\end{pmatrix} \begin{pmatrix}j_x\\j_y\end{pmatrix}
\end{equation}
Die Größe \(\tau\) bezeichnet hierbei die Impulsrelaxationszeit, die ein Maß für die Stärke von Streuprozessen ist. Betrachtet man den Fall kleiner Magnetfelder (\(\omega_c \tau \ll 1\)), so lässt sich nun eine Gleichung aufstellen, die Ladungsträgerdichte \(n_e\) und die Querspannung \(U_{xy}\) verbindet:
\begin{equation}
	\label{ne_abl}
	n_e = \frac{I/e}{dU_{xy}/dB}
\end{equation}
Die Beweglichkeit ergibt sich zu
\begin{equation}
\label{beweglichkeiteq}
	\mu_e = \frac{\sigma_0}{e n_e}.
\end{equation}
Bei sehr großen Magnetfeldern (\(\omega_c\tau \gg 1\)) erhält man, dass die Hall-Spannung identisch mit der Source-Drain-Spannung ist. Außerdem lässt sich der Bahnradius des kreisförmigen Anteils der Trajektorie eines Elektrons mit
\begin{equation}
\label{radius}
R_c = \frac{\hbar}{eB}\sqrt{2\pi n_e}
\end{equation}
berechnen.
\subsection{Antidots}
Antidots sind kleine, nichtleitende Bereiche, die die Elektronenbahnen beeinflussen können. Bei bestimmten Werten des Magnetfeldes kommt es zu geschlossenen Bahnen, was zu einer Lokalisierung der Elektronen und damit zu einem erhöhten elektrischen Widerstand führt. Bei Magnetfeldern, die den Zyklotronradius kleiner als die Gitterkonstante des Antidotgitters machen, kommt es zu den sogenannten Runaway-Trajektorien, die zu einem geringerem Längswiderstand führen.

\subsection{2DEG}
Ein 2DEG ist ein Elektronengas mit zweidimensionaler Ausdehnung. Man kann ein solches erzeugen, indem man z.B. n-dotiertes AlGaAs mit GaAs verbindet: Aufgrund der unterschiedlichen Bandstrukturen ergibt sich am Materialübergang ein in z-Richtung sehr dünner Potentialtopf, der effektiv die Bewegung der Elektronen auf 2 Dimensionen einschränkt.
\subsection{Energiequantisierung im Magnetfeld}
Legt man ein Magnetfeld an, so ergibt sich aufgrund der Drehimpulsquantisierung eine Quantisierung der Energien, die durch die Landau-Niveaus gegeben sind:
\begin{equation}
\epsilon_n = \hbar \omega_c \left( n + \frac{1}{2} \right).
\end{equation}
Zusätzlich existiert wegen des Elektronenspins noch eine weitere Unteraufspaltung durch den Zeemaneffekt. Die sich ergebende Zustandsdichte ist überall verschwindend, bis auf Energien, die sich nahe an den Landau-Niveaus befinden (Abb. \ref{landau}); Landau-Niveaus, die sich unterhalb der Fermienergie befinden, sind bei \(T=0\) komplett befüllt, während solche, die sich bei höheren Energien befinden, komplett leer sind. Da die Landauniveaus proportional zu \(B\) sind, sinkt die Anzahl der besetzten Niveaus mit steigendem Magnetfeld. Dies wird beschrieben durch den Füllfaktor
\begin{equation}
\label{nu}
\nu = \frac{n_e h}{e B}.
\end{equation}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{landau.png}
	\caption{Die Zustandsdichte eines 2DEG im Magnetfeld}
	\label{landau}
\end{figure}

Es muss erwähnt werden, dass die Zustandsdichte aufgrund von Defekten nicht eine Summe von Delta-Peaks ist, sondern dass diese Peaks wesentlich verbreitert sind, wodurch eventuell die zu den beiden Spin-Ausrichtungen gehöhrenden Peaks nicht mehr aufgelöst werden können. Die Modulation der Energieverteilung aufgrund der Defekte führt in vollbesetzten Landau-Niveaus zu einer starken Lokalisierung.

\subsection{Randkanäle}
In der Realität hat die Probe eine endliche Ausdehnung, was zu einem sehr hohen Potential an den Rändern der Probe führt. Dies führt zu einer "`Reflektion" der Elektronen am Rand, was zu widerstandsfreien Randkanälen führt. Die Anzahl dieser Kanäle entspricht der Zahl der Landauniveaus, deren Energien unterhalb der Fermienergie liegen. Der Querwiderstand pro Randkanal beträgt
\begin{equation}
R_{\text{xy}} = \frac{h}{e^2}.
\end{equation}
Falls die Fermienergie genau innerhalb eines Landau-Niveaus liegt, so können die dort enthaltenen Zustände thermisch streuen. Dies hebt die Lokalisierung auf und das gesamte 2DEG trägt zum Stromtransport bei. Paradoxerweise ist in diesem Fall der Längswiderstand jedoch höher als wenn \(E_f \neq E_{\text{Landau}}\), da die Streuung auch mit den Randkanälen stattfindet, was dazu führt, dass sie nicht mehr widerstandsfrei sind.\\
Dies führt zu den \textbf{Shubnikov-de-Haas-Oszillationen}: Der Längswiderstand fällt immer dann ab, wenn das Ferminiveau kein Landauniveau trifft, da dann die widerstandsfreien Randkanäle den Strom tragen. Liegt die Fermienergie in einem Landau-Niveau, so findet Streuung auch in den Randkanälen statt und der Längswiderstand steigt an.

Die Maxima des Längswiderstandes treten bei halbzahligem Füllfaktor auf; dadurch kann man die Ladungsträgerdichte durch Kenntnis der Magnetfelder zweier aufeinanderfolgender Maxima der Längswiderstände berechnen:
\begin{equation}
\label{minmax}
n_e = \frac{e}{h} \frac{1}{\frac{1}{B_i}-\frac{1}{B_{i+1}}}.
\end{equation}
\subsection{Quanten-Hall-Effekt}
Bei Magnetfeldern, die zu einem verschwindenden Längswiderstand führen, misst man gleichzeitig Plateaus in der Hall-Spannung. Die entsprechenden Hallwiderstände lassen sich mittels
\begin{equation}
\label{qhe}
R_H=\frac{R_K}{\nu}
\end{equation}
mit ganzzahligem \(\nu\) ermitteln, wobei die Klitzing-Konstante \(R_K\  \SI{25.8}{\kilo\ohm}\) beträgt.
\section{Versuch}
\subsection{Aufbau}
In diesem Versuch wurde eine GaAs/AlGaAs-Heterostruktur in einem Vakuumrohr, das mit flüssigem Helium gekühlt wurde, als Probe verwendet. Die Probe bestand aus zwei Teilen: Eine war unstrukturiert, die andere wies ein Übergitter aus runden Antidots auf (Gitterkonstante \(750\,nm\)). Die Probe wurde an eine Spannungsquelle angeschlossen, um einen Strom fließen zu lassen. Da jedoch keine Konstantstromquelle zur Verfügung stand, wurde ein Vorwiderstand von \(\SI{1}{\mega\ohm}\) in Serie geschaltet und dann die Spannung auf \(\SI{2}{\volt}\) eingestellt, damit -- unter Vernachlässigung des Probenwiderstandes -- ein Strom von \(\SI{2}{\micro\ampere}\) floss. Kontakte an den Seiten der Probe ermöglichten es, Spanungsabfälle parallel sowie senkrecht zur Stromrichtung in beiden Bereichen zu messen (Verstärkung mit Differenzverstärker um den Faktor 100, Verhältnis von Abstand der Längs- und Quermessung l/b  ungefähr \(0.75\)). Mit Hilfe einer Magnetspule aus supraleitendem NbTi-Draht konnte ein Magnetfeld senkrecht zur Probe erzeugt werden (Spulenstrom \SIrange{0}{25}{\ampere}, Spulenkonstante \(\SI{0.226}{\tesla\per\ampere}\)). Eine Gatterspannung konnte angelegt werden, um den Potentialverlauf in der Probe und damit die Ladungsträgerdichte variieren zu können.

Die Kontakte zur Spannungsmessung an der Probe sowie Kontake des Magnetnetzgerätes wurden mit einem XY-Schreiber verbunden, der bei Veränderung des Magnetfeldes die dazugehörige Messspannung aufzeichnen konnte.

\subsection{Durchführung}
Es wurde für beide Bereiche der Probe sowohl der Spannungsabfall in Längsrichtung, als auch in Querrichtung für ein durchfahrendes Magnetfeld gemessen, wobei jeweils 3 verschiedene Gate-Spannungen angelegt wurden: \(U_g \in \{-0.15, 0, 0.5\}V\)
\subsection{Auswertung}
Da kein ausreichend großer Scanner zur Verfügung stand, wurden die Messsergebnisse zweiseitig eingescannt und dann mit Hilfe eines Bildbearbeitungsprogramms zusammengefügt:
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{S.jpg}
    \caption{Die Ergebnisse für die Messung im strukturierten Bereich: Die x-Achse ist proportional zum Magnetfeld, die y-Achse zur Messspannung. Die schwarze (\(\SI{0.2}{\volt\per\centi\metre}\)), orangene (\(\SI{0.2}{\volt\per\centi\metre}\)) und pinke (\(\SI{0.1}{\volt\per\centi\metre}\)) Kurve ist die Querspannung und die rote (\(\SI{0.1}{\volt\per\centi\metre}\)), violette (\(\SI{0.5}{\milli\volt\per\centi\metre}\)) und blaue (\(\SI{5}{\milli\volt\per\centi\metre}\)) die Längsspannung mit der jeweiligen Gate-Spannung \(\SI{-0.15}{\volt}\), \(\SI{0}{\volt}\) und \(\SI{0.5}{\volt}\). Die x-Empfindlichkeit betrug \(\SI{2}{\milli\volt}/\SI{}{\centi\metre}\). Die rote Kurve gehört zu einer fehlerhaften Messung und wurde ignoriert.}
    \label{spektrum_174}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{US.jpg}
    \caption{Die Ergebnisse für die Messung im unstrukturierten Bereich: Die x-Achse ist proportional zum Magnetfeld, die y-Achse zur Messspannung. Die blaue (\(\SI{0.2}{\volt\per\centi\metre}\)), schwarze (\(\SI{0.1}{\volt\per\centi\metre}\)) und grüne (\(\SI{0.1}{\volt\per\centi\metre}\)) Kurve ist die Querspannung und die rote (\(\SI{0.01}{\volt\per\centi\metre}\)), violette (\(\SI{5}{\milli\volt\per\centi\metre}\)) und pinke (\(\SI{5}{\milli\volt\per\centi\metre}\)) die Längsspannung mit der jeweiligen Gate-Spannung \(\SI{-0.15}{\volt}\), \(\SI{0}{\volt}\) und \(\SI{0.5}{\volt}\). Die x-Empfindlichkeit betrug \(\SI{2}{\milli\volt}/\SI{}{\centi\metre}\). Die untere violette und rote Kurve gehören zu fehlerhaften Messungen und wurden ignoriert.}
	\label{spektrum_174}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{detail.jpg}
	\caption{Detailaufnahme der Längsspannung im strukturierten Bereich: Grün: \(\SI{5}{\milli\volt\per\centi\metre}, V_g=\SI{-0.15}{\volt}\); Schwarz: \(\SI{2}{\milli\volt\per\centi\metre}, V_g=\SI{0}{\volt}\); Blau: \(\SI{5}{\milli\volt\per\centi\metre}, V_g=\SI{0.5}{\volt}\).  Die x-Empfindlichkeit betrug \(\SI{0.5}{\milli\volt}/\SI{}{\centi\metre}\).}
	\label{spektrum_174}
\end{figure}
\newpage
Da die Messwerte aufgrund der verschiedenen Verstärkungsfaktoren der einzelnen Kurven nicht gut vergleichbar sind, wurde das Programm PlotDigitizer verwendet, um die Daten zu digitalisieren. Mit Hilfe der Verstärkungsfaktoren und der Spulenkonstante \(\SI{0.226}{\tesla\per\ampere}\) konnten die Daten in die Einheiten Volt und Tesla umgerechnet werden. Diese digitalisierten Daten wurden jedoch nur zur Visualisierung verwendet; da die Digitalisierung eine weitere mögliche Ursache für Messfehler darstellt, wurden genaue Messungen immer direkt auf dem Millimeterpapier vorgenommen.

Im Folgenden wird folgendes Benennungsschema für die einzelnen Datensätze verwendet: Eine Zahl gibt die Gatespannung an, der Buchstabe \textbf{S} steht für eine Messung im strukturierten Bereich, während \textbf{US} für eine Messung im unstrukturierten Bereoich steht. \textbf{-0.15S} ist also der Bezeichner von \(V_g=\SI{-0.15}{\volt}\) im strukturierten Bereich.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{quer.png}
	\caption{Die Abhängigkeit der Querspannung vom Magnetfeld. Gestrichelte Kurven sind Messungen im unstrukturierten Bereich, durchgezogene Linien im strukturierten Bereich.}
	\label{quer}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{laengs.png}
	\caption{Die Abhängigkeit der Längsspannung vom Magnetfeld. Gestrichelte Kurven sind Messungen im unstrukturierten Bereich, durchgezogene Linien im strukturierten Bereich.}
	\label{laengs}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{laengs-detail.png}
	\caption{Detailmessung der Längsspannung im strukturierten Bereich.}
	\label{detail}
\end{figure}

\subsubsection{Fehlerabschätzung}
Folgende Fehlerquellen wurden berücksichtigt:
\begin{itemize}
	\item \textbf{Stromveränderung}: Es wurde angenommen, dass der Widerstand der Probe im Vergleich zum Vorwiderstand von \(\SI{1}{\mega\ohm}\) vernachlässigbar ist, und dass damit der Strom durch die Probe konstant ist, unabhängig vom anliegenden Magnetfeld. Es wurde jedoch gemessen, dass bei vollständigem Durchfahren des Magnetfeldes die Spannung, die am Vorwiderstand anliegt, von \(\SI{2}{\volt}\) auf \(\SI{1.967}{\volt}\) abfällt. Dies bedeutet, dass der Widerstand der Probe sich geändert hat:
	\begin{align*}
		R_{\text{probe}}& = R_{\text{res}} \cdot \frac{U_{\text{probe}}}{U_{\text{res}}},\\
		R_{\text{ges}} &= R_{\text{res}} + R_{\text{probe}},\\
		I &= \frac{U_{\text{ges}}}{R_{\text{ges}}}\\
		  &= \frac{\SI{2}{\volt}}{\SI{1}{\mega\ohm} \left( 1 + \frac{\SI{0.033}{\volt}}{\SI{1.967}{\volt}} \right)}\\
		  &= \SI{1.967}{\micro\ampere}
	\end{align*}
	Dieser Wert für den Strom bei vollem Magnetfeld ist kleiner als der Ursprungswert von \(\SI{2}{\micro\ampere}\). Deswegen wurde bei der Auswertung \(I=\SI{1.985\pm 0.015}{\micro\ampere}\) verwendet.
	\item \textbf{Ablesegenauigkeit}: Die Ablesegenauigkeit auf dem Millimeterpapier beträgt aufgrund der verwendeten Strichbreite etwa \(\SI{1}{\milli\metre}.\). In manchen Teilversuchen war die Ablesegenauigkeit jedoch wesentlich geringer, da die zu untersuchenden Kurven nicht eindeutig waren. Darauf wird in den entsprechenden Abschnitten gesondert hingewiesen.
	\item \textbf{Ungenauigkeit des X-Y-Schreibers:} Die Aufzeichnungsgenauigkeit des X-Y-Schreibers wurde auf \(\SI{1}{\milli\meter}\) geschätzt.
\end{itemize}

Desweiteren tragen möglicherweise folgende Fehlerquellen zum Ergebnis bei, die jedoch nicht quantifiziert werden konnten, und deswegen nicht in die Fehlerrechnungen einbezogen wurden:
\begin{itemize}
	\item Die Spulenkonstante ist eventuell nicht genau bestimmt
	\item Die Gatespannungen wurden eventuell nicht genau eingestellt
	\item Das Magnetfeld kann um einen Offset nach oben oder unten verschoben sein
\end{itemize}

\subsubsection{Ermittlung der Ladungsträgerdichte mittels Betrachtung der Hallspannung}
Die in Abb. \ref{quer} abgebildeten Kurven der Hallspannung entsprechen den Erwartungen: Bei niedrigen Magnetfeldern existiert ein linearer Zusammenhang zwischen Hall-Spannung und Magnetfeld, genau wie es klassisch erwartet wird. Bei hohen Magnetfeldern tritt der Quanten-Hall-Effekt auf, der zu den gemessenen Plateaus führt. Es ist außerdem auffällig, dass die gemessenen Hall-Spannungen größer sind, je kleiner die Gatterspannung ist. Dies ist im Einklang mit der Theorie des Hall-Effekts, da dessen Stärke proportional zum Inversen der Ladungsträgerdichte ist, und diese mit steigender Gatterspannung zunimmt.


Wie im Theorieteil beschrieben, ist die Ladungsträgerdichte bei geringen Werten von \(B\) indirekt proportional zur Ableitung der Hallspannung nach dem Magnetfeld (siehe Gleichung \ref{ne_abl}). Es wurde deswegen an die Daten aus Abb. \ref{quer} für Werte mit \(B < \SI{1.5}{\tesla}\) eine Ursprungsgerade gefittet. Die so erhaltenen Steigungen wurden mittels Gleichung \ref{ne_abl} in Ladungsträgerdichten umgerechnet. Der Fehler wurde mit Hilfe Gauß'scher Fehlerfortpflanzung errechnet, wobei die Ungenauigkeit des Stromwertes sowie die Unsicherheit des Fits mit einbezogen wurden:

\input{ne}

Es ergibt sich, dass sich mit steigender Gatespannung die Ladungsträgerdichte erhöht. Dies entspricht den Erwartungen, da das Anlegen einer positiven Spannung zu einer Attraktion von Elektronen führt.

\subsubsection{Füllfaktoren}
Nun wurden die Plateaus des Hallwiderstands betrachtet: Es wurde sowohl der Magnetfeldwert als auch die korrespondierende Hallspannung aus den Plots gemessen. Aufgrund von Gleichung \ref{qhe} müssen die Hall-Spannungen folgende Gleichung erfüllen:
\begin{equation}
	U_\nu = \frac{U_{\text{max}}}{\nu}.
\end{equation}
Im Experiment zeigt sich wie erwartet, dass die Werte der Hallspannung genau eine solche Abhängigkeit auswiesen; die Füllfaktoren für die einzelnen Plateaus wurden nun ermittelt, indem überprüft wurde, ob die obenstehende Gleichung erfüllt ist (\(U_{\text{max}}\) war anfangs nicht bekannt, die von-Klitzing-Konstante wurde nicht verwendet; es wurden die Verhältnisse der Plateauwerte betrachtet und daraus Rückschlüsse auf die Füllfaktoren gezogen).

\begin{itemize}
\input{plateau}
\end{itemize}

Mit Hilfe der Füllfaktoren konnte außerdem \(U_{\text{max}} = U \nu\) bestimmt werden; diese Größe hängt wie folgt mit der Querspannung zusammen:
\begin{equation}
R_{\text{max}} = \frac{U_{\text{max}}}{I}
\end{equation}
\(R_{\text{max}}\) sollte genau der Klitzing-Konstante \((R_K)\) entsprechen. Diese Größe konnte nun bestimmt werden (gemittelt über die 6 verschiedenen Fälle):
\begin{equation}
R_K^{\text{exp}} = \SI{27\pm 1}{\kilo\ohm}
\end{equation}
Dies stimmt nahezu innerhalb der Fehlergrenzen mit dem Literaturwert von \(\SI{25.8}{\kilo\ohm}\) überein. Vermutlich liegt also noch ein systematischer Fehler vor, wie später erläutert werden wird.

Um zusätzlich zu überprüfen, ob die erhaltenen Füllfaktoren realistisch sind, wurde Gleichung \ref{nu} unter Verwendung der im vorherigen Abschnitt ermittelten Ladungsträgerdichten geplottet, und anschließend die in diesem Versuch ermittelten Füllfaktorpunkte in den gleichen Graph eingezeichnet (Abb. \ref{fuellfaktorus} und \ref{fuellfaktors}). Der Fehler wurde mittels Gaußscher Fehlerfortpflanzung berechnet, nachdem die Ungenauigkeit beim Ablesen des Plateauwertes geschätzt wurde. Hierbei wurden für diese Messungenauigkeit Werte von etwa \(\SI{5}{\milli\metre}\) verwendet, da die genaue Position des Maximums teilweise schwer ersichtlich war.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{fuellfaktor_s.png}
	\caption{Füllfaktor im strukturierten Bereich.}
	\label{fuellfaktors}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{fuellfaktor_us.png}
	\caption{Füllfaktor im unstrukturierten Bereich.}
	\label{fuellfaktorus}
\end{figure}


Es ergibt sich insgesamt eine gute Übereinstimmung; einige Messpunkte stimmen zwar im Rahmen der Messgenauigkeit nicht mit der geplotteten Kurve überein, jedoch ist hierbei zu beachten, dass diese auf Basis eines ebenfalls ungenau bestimmten Wertes, nämlich der Ladungsträgerdichte, bestimmt wurde, und damit ebenfalls einer Unsicherheit unterliegt.
Es fällt jedoch auf, dass die Punkte nicht zufällig um die eingezeichneten Kurven verteilt sind, sondern sich alle rechts von ihnen befinden. Dies deutet auf einen systematischen Fehler hin. Dafür käme ein Fehler in der Spannungsmessung oder bei der Bestimmung des Magnetfeldes in Frage. Ersteres wurde für unwahrscheinlich erachtet, da die Spannungsmessung eigentlich sehr genau erfolgen kann. Deswegen wird im Folgenden angenommen, dass das tatsächliche Magnetfeld einen anderen Wert als vermutet hat. Hierfür kommt
\begin{itemize}
	\item ein Offset
	\item ein anderer Skalierungsfaktor (Spulenkonstante)
\end{itemize}
in Frage. Betrachtet man Gleichungen \ref{minmax} und \ref{nu}, so sieht man, dass sich ein anderer Skalierungsfaktor kürzen würde, und somit nichts an der "Verschiebung" der Punkte nach rechts ändern würde. Deswegen wurde angenommen, dass ein ungenügend korrigierter Offset des Magnetfeldes vorliegt. Erstellt man die Abbildungen \ref{fuellfaktors} und \ref{fuellfaktorus} noch einmal, mit der Annahme eines um \(\SI{0.2}{\tesla}\) erhöhten Magnetfeldes, so ergibt sich eine wesentlich bessere Übereinstimmung der Kurven mit den Punkten (Abbildungen \ref{fuellfaktoruskorr} und \ref{fuellfaktorskorr}).

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{fuellfaktor_s_korr.png}
	\caption{Füllfaktor im strukturierten Bereich (korrigiertes Magnetfeld).}
	\label{fuellfaktorskorr}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{fuellfaktor_us_korr.png}
	\caption{Füllfaktor im unstrukturierten Bereich (korrigiertes Magnetfeld).}
	\label{fuellfaktoruskorr}
\end{figure}


\subsubsection{Ladungsträgerdichte aus SdH-Oszillationen}

Die Position der Maxima (halbzahliger Füllfaktor) und Minima (ganzzahliger Füllfaktor) der Längsspannung wurden aus den Daten gemessen und in die Einheit Tesla umgerechnet. Mit Hilfe der Ergebnisse des vorigen Abschnitts wurden ihnen dann die entsprechenden Füllfaktoren zugewiesen:
\newpage
\newpage
\newgeometry{left=1cm,bottom=0.1cm}
\begin{itemize}
\input{sdh}
\end{itemize}

\newpage
\restoregeometry

Anschließend wurden die entsprechenden Füllfaktoren als Funktion von \(1/B\) aufgetragen (Abb. \ref{ne_fuell}). Aufgrund von Gleichung \ref{nu} wurden Ursprungsgeraden erwartet, was sich gut bestätigte. Es wurde nun jeweils für alle aufeinanderfolgenden Minima / Maxima mit Hilfe von Gleichung \ref{minmax} die Ladungsträgerdichte berechnet und schließlich der Mittelwert gebildet; der Fehler wurde mittels gaußscher Fehlerfortpflanzung berechnet.

\begin{table}[H]
\begin{tabular}{ll}
\textbf{Name}	&  \textbf{Ladungsträgerdichte \([\SI{}{\metre^{-3}}]\)}  \\
\input{ne_fuell}
\end{tabular}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{n_e.png}
	\caption{Punkte stehen für den strukturierten Bereich, während Sterne für den unstrukturierten Bereich stehen.}
	\label{ne_fuell}
\end{figure}

\subsubsection{Ermittlung der Beweglichkeit}
Zur Ermittlung der Beweglichkeit der Elektronen wurde die Längsspannung bei \(B=0\) abgelesen und in Spannungen umgerechnet. Anschließend konnte mittels Gleichung \ref{beweglichkeiteq} die Beweglichkeit der Elektronen berechnet werden. Die Werte wurden in Abbildung \ref{beweglichkeit} eingezeichnet.
\input{beweglichkeit}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{beweglichkeit.png}
	\caption{Werte für den unstrukturierten Bereich sind in rot, Werte für den strukturierten Bereich in blau.}
	\label{beweglichkeit}
\end{figure}
Zuerst einmal ist festzuhalten, dass sehr hohe Beweglichkeiten beobachtet wurden, was im Einklang mit Forschungsergebnissen über 2DEGs steht\footnote{siehe http://adsabs.harvard.edu/abs/2009JCrGr.311.1658U}.
Es wurde beobachtet, dass die Beweglichkeit mit steigender Gatespannung zunimmt. Wie in den vorherigen Abschnitten festgestellt führt eine höhere Gatespannung ebenfalls zu einer höheren Ladungsträgerdichte; dies bedeutet, dass die Beweglichkeit höher ist, je größer die Ladungsträgerdichte ist. Dies ist aufgrund folgender Gleichung verwunderlich (\(m\) ist die effektive Masse der Elektronen):

\begin{align*}
	\mu &= \frac{\sigma}{e n_e}\\
		&= \frac{n_e e^2 \tau}{m} \frac{1}{e n_e}\\
		&= \frac{e \tau}{m},
\end{align*}

Die mittlere Zeit zwischen zwei Stößen \(\tau\) sollte bei steigender Ladungsträgerdichte geringer werden, da nun mehr Ladungsträger vorhanden sind, zwischen denen Stöße stattfinden können. Um dies auszugleichen, müsste also die effektive Elektronenmasse sehr stark bei höheren Ladungsträgerdichten abnehmen, um einen effektiven Anstieg der Beweglichkeit zu erreichen. Ein Anstieg der Beweglichkeit mit der Ladungsträgerdichte wurde in wissenschaftlichen Veröffentlichungen ebenfalls berichtet\footnote{siehe https://arxiv.org/pdf/1109.6911v1.pdf}.

Möglicherweise kann die Beobachtung, dass sich die Beweglichkeit bei höheren Gatespannungen erhöht, folgendermaßen erklärt werden: Die Elektronen im 2DEG üben aufeinander eine abstoßende Kraft aus, die unter Anderem transversal zur Stromrichtung wirkt, die Elektronen also auf den Rand des 2DEGs zutreibt. Dies führt offensichtlich zu einer verminderten Beweglichkeit, da die Elektronen nun mit dem Randpotential des 2DEGs streuen. Ist jedoch eine positive Gatespannung angelegt, so kann diese der auseinandertreibenden Kraft entgegenwirken, was effektiv die Beweglichkeit erhöht.

Die Tatsache, dass der Anstieg der Beweglichkeit im strukturierten Bereich wesentlich schwächer aus als im unstrukturierten Bereich ausfällt, lässt sich dadurch erklären, dass es im strukturierten Bereich zur Lokalisierung von Elektronen um die Antidots herum kommt. Diese wirken als zusätzliche Streuzentren, die Beweglichkeit vermindernd.

\newpage
\subsubsection{Vergleich der Werte für die Ladungsträgerdichte}
Nun wurden die Ladungsträgerdichten, die mittels zwei verschiedener Methoden ermittelt wurden, verglichen (Abbbildungen \ref{ne_vergleich_s} und \ref{ne_vergleich_us}).

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{vergleich_ne_s.png}
	\caption{Vergleich der Ladungsträgerdichten im strukturierten Bereich. Rote Punkte symbolisieren Werte, die mittels der ersten Methode ermittelt wurden, blaue Sterne solche, die mit der zweiten Methode gefunden wurden.}
	\label{ne_vergleich_s}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{vergleich_ne_us.png}
	\caption{Vergleich der Ladungsträgerdichten im unstrukturierten Bereich. Rote Punkte symbolisieren Werte, die mittels der ersten Methode ermittelt wurden, blaue Sterne solche, die mit der zweiten Methode gefunden wurden.}
	\label{ne_vergleich_us}
\end{figure}

Es fällt auf, dass die mit den verschiedenen Methoden ermittelten Werte eine ähnliche Abhängigkeit von der Gatespannung aufweisen: Je höher diese ist, desto höher ist auch die Ladungsträgerdichte. Der Grund dafür ist, dass eine positive Gatterspannung auf die Elektronen anziehend wirkt.
Jedoch scheint wie schon bei einem vorherigen Teilversuch ein Offset vorzuliegen. Korrigiert man wiederum das Magnetfeld um \(\SI{0.2}{\tesla}\) nach oben, so ergibt sich eine wesentlich bessere Übereinstimmung (Abbildungen \ref{ne_vergleich_s_korr} und \ref{ne_vergleich_us_korr}).

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{vergleich_ne_s_korr.png}
	\caption{Vergleich der Ladungsträgerdichten im strukturierten Bereich bei korrigiertem Magnetfeld. Rote Punkte symbolisieren Werte, die mittels der ersten Methode ermittelt wurden, blaue Sterne solche, die mit der zweiten Methode gefunden wurden.}
	\label{ne_vergleich_s_korr}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{vergleich_ne_us_korr.png}
	\caption{Vergleich der Ladungsträgerdichten im unstrukturierten Bereich bei korrigiertem Magnetfeld. Rote Punkte symbolisieren Werte, die mittels der ersten Methode ermittelt wurden, blaue Sterne solche, die mit der zweiten Methode gefunden wurden.}
	\label{ne_vergleich_us_korr}
\end{figure}

\section{Detailuntersuchung des strukturierten Bereichs}
\subsection{Identifizierung der Elektronenbahnen im Antidotgitter}
Betrachtet man die Kurven der Längsspannung im Detail, so fällt auf, dass sich im strukturierten Bereich bei niedrigen Magnetfeldern Widerstandsmaxima ergeben, die nicht durch SdH-Oszillationen erklärt werden können. Dies lässt sich dadurch erklären, dass es bei geordneter Struktur stabile Bahnen um einen oder mehrere Antidots gibt, die zu einer Lokalisation der Elektronen und damit einer Erhöhung des Widerstandes führen. In Abb. \ref{detail} wurden folgende Maxima ausgemacht:
\input{detail_max}
Mit Gleichung \ref{radius} lässt sich mit der in den vorherigen Teilaufgaben ermittelten Ladungsträgerdichte diesen Magnetfeldern ein Elektronenbahndurchmesser zuordnen:

\input{radius}

Der kleinste Bahndurchmesser liegt bei allen drei Spannungen in der Größenordnung von \(a=\SI{750}{\nano\metre}\), was einer Kreisbahn um einen einzigen Antidot entspricht (in Abb. \ref{bahnen} Kurve A). 
Der nächstgrößere Durchmesser wurde wie die meisten weiteren nur bei \(\SI{0.5}{\volt}\) eindeutig beobachtet: Mit knapp \(\SI{1e-6}{\metre}\) entspricht er einer Bahn, die zwei Dots einschließt (B). Anschließend folgt eine Bahn, die vier Dots umläuft (C), bevor es bei etwa \(\SI{2250}{\nano\metre}\) eine Bahn gibt, die 9 Antidots einschließt (D). Die größeren Bahnen wurden nicht mehr eingezeichnet.

Es ist zu erwähnen, dass die Oszillationen bei einer Gatespannung von \(\SI{0.5}{\volt}\) am stärksten ausfielen und bei \(\SI{-0.15}{\volt}\) nur noch sehr schwer auszumachen waren. Eine Erklärung für dieses Phänomen wird später beschrieben.
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{kreise.jpg}
	\caption{Beobachtete Elektronentrajektorien}
	\label{bahnen}
\end{figure}


\subsection{Dominanz der Runaway-Trajektorien}

Wie im Theorieteil erläutert wird erwartet, dass Runaway-Trajektorien dominieren, wenn der Radius der Zyklotronbahnen kleiner als die Hälfte des Gitterabstandes \(a=\SI{750}{\nano\metre}\) abzüglich der anhand des AFM-Bildes von Probe B1\footnote{Es wurde angenommen, dass die Antidots der Probe D2 ähnliche Ausdehnungen haben wie die der Probe B1, auch wenn dies in der Versuchsanleitung nicht explizit erwähnt wurde.} geschätzten Ausdehnung der Antidots von \(\SI{250}{\nano\metre}\) ist. Mit Gleichung \ref{radius} ergibt sich für das kritische Magnetfeld:
\begin{equation}
B_{\text{krit}} = \frac{\hbar}{e\cdot 0.5\cdot a} \sqrt{2\pi n_e}.
\end{equation}

Unter Verwendung der in den vorherigen Abschnitten ermittelten Ladungsträgerdichten ergibt sich:
\input{crit_magn}
Es wurde erwartet, dass die gemessene Längsspannung in Abb. \ref{detail} ab diesem Wert stark abfällt, was auch im Experiment der Fall ist. Wie erwartet zeigt sich im dort auch, dass Runaway-Trajektorien bei höheren Gatespannungen bei höheren Magnetfeldern dominieren.

\subsection{Abhängigkeit der Elektronenbahnen von der Gatterspannung}
Es wurde beobachtet, dass die Oszillationen stärker waren, je höher die Gatespannung war. Dies lässt sich verstehen, wenn man berücksichtigt, dass eine positive Gatterspannung die Fermienergie nach unten verschiebt. Für die nun energetisch niedriger liegenden Elektronen erscheint nun das Antidotpotential höher. Die Tatsache, dass die Oszillationen bei einer Gatespannung von \(\SI{-0.15}{\volt}\) kaum mehr sichtbar waren, lässt den Rückschluss zu, dass das Antidotgitterpotential sich in der Größenordnung der verwendeten Spannungen befinden muss (\(\simeq\SI{1}{\volt}\)). 
\end{document}
