\documentclass[12pt, a4paper, ngerman]{scrartcl}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{babel}
\usepackage{a4wide}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage{braket}
\usepackage{gensymb}
\usepackage{csvsimple}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{float}
\usepackage{hyperref}
\usepackage{breqn}
\usepackage{float}


\sisetup{range-units=single, separate-uncertainty=true}


\title{Auswertung K1}
\subtitle{Quanten-Hall-Effekt}
\author{Jannik Luhn, Benjamin Wiegand}


\newcommand{\Spvek}[2][r]{%
	\gdef\@VORNE{1}
	\left(\hskip-\arraycolsep%
	\begin{array}{#1}\vekSp@lten{#2}\end{array}%
	\hskip-\arraycolsep\right)}

\def\vekSp@lten#1{\xvekSp@lten#1;vekL@stLine;}
\def\vekL@stLine{vekL@stLine}
\def\xvekSp@lten#1;{\def\temp{#1}%
	\ifx\temp\vekL@stLine
	\else
	\ifnum\@VORNE=1\gdef\@VORNE{0}
	\else\@arraycr\fi%
	#1%
	\expandafter\xvekSp@lten
	\fi}

\begin{document}
\maketitle

In dem hier ausgewerteten Praktikumsversuch soll der Quanten-Hall-Effekt untersucht werden.


\section{Theoretische Grundlagen}



\subsection{Klassischer Hall-Effekt}
Fließt ein Strom \(I\) durch einen elektrischen Leiter, der senkrecht von einem Magnetfeld durchdrungen ist, so wirkt auf die Ladungsträger die Lorentzkraft, die schließlich zu einer Querspannung -- der Hall-Spannung -- führt.

Betrachtet man einen freien Ladungsträger (Ladung \(e\), Masse \(m\)), der sich sowohl in einem elektrischen Feld als auch in einem darauf senkrecht stehendem Magnetfeld \(B\) befindet, so bewegt dieser sich auf sogenannten Zyklotronbahnen, wobei die Zyklotronfrequenz
\[
\omega_c = \frac{e B}{m}
\]
beträgt. Mit Hilfe des Drude-Modells lässt sich die Bewegungsgleichung in Form des ohmschen Gesetzes schreiben:
\begin{equation}
\label{ohm}
	\begin{pmatrix}E_x\\E_y\end{pmatrix} = \sigma_0^{-1} \begin{pmatrix}1\quad -\omega_c \tau\\\omega_c \tau\quad 1\end{pmatrix} \begin{pmatrix}j_x\\j_y\end{pmatrix}
\end{equation}
Die Größe \(\tau\) bezeichnet hierbei die Impulsrelaxationszeit, die ein Maß für die Stärke von Streuprozessen ist. Betrachtet man den Fall kleiner Magnetfelder (\(\omega_c \tau \ll 1\)), so lässt sich nun eine Gleichung aufstellen, die Ladungsträgerdichte \(n_e\) und die Querspannung \(U_{xy}\) verbindet:
\begin{equation}
	\label{ne_abl}
	n_e = \frac{I/e}{dU_{xy}/dB}
\end{equation}
Die Beweglichkeit ergibt sich zu
\begin{equation}
\label{beweglichkeit}
	\mu_e = \frac{\sigma_0}{e n_e}.
\end{equation}
Bei sehr großen Magnetfeldern (\(\omega_c\tau \gg 1\)) erhält man, dass die Hall-Spannung identisch mit der Source-Drain-Spannung ist. Außerdem lässt sich der Bahnradius des kreisförmigen Anteils der Trajektorie eines Elektrons mit
\begin{equation}
\label{radius}
R_c = \frac{\hbar}{eB}\sqrt{2\pi n_e}
\end{equation}
berechnen.
\subsection{Antidots}
Antidots sind kleine, nichtleitende Bereiche, die die Elektronenbahnen beeinflussen können. Bei bestimmten Werten des Magnetfeldes kommt es zu geschlossenen Bahnen, was zu einer Lokalisierung der Elektronen und damit zu einem erhöhten elektrischen Widerstand führt. Bei Magnetfeldern, die den Zyklotronradius kleiner als die Gitterkonstante des Antidotgitters machen, kommt es zu den sogenannten Runaway-Trajektorien, die zu einem geringerem Längswiderstand führen.

\subsection{2DEG}
Ein 2DEG ist ein Elektronengas mit zweidimensionaler Ausdehnung. Man kann ein solches erzeugen, indem man z.B. n-dotiertes AlGaAs mit GaAs verbindet: Aufgrund der unterschiedlichen Bandstrukturen ergibt sich am Materialübergang ein in z-Richtung sehr dünner Potentialtopf, der effektiv die Bewegung der Elektronen auf 2 Dimensionen einschränkt.
\subsection{Energiequantisierung im Magnetfeld}
Legt man ein Magnetfeld an, so ergibt sich aufgrund der Drehimpulsquantisierung eine Quantisierung der Energien, die durch die Landau-Niveaus gegeben sind:
\begin{equation}
\epsilon_n = \hbar \omega_c \left( n + \frac{1}{2} \right).
\end{equation}
Zusätzlich existiert wegen des Elektronenspins noch eine weitere Unteraufspaltung durch den Zeemaneffekt. Die sich ergebende Zustandsdichte ist überall verschwindend, bis auf Energien, die sich nahe an den Landau-Niveaus befinden (Fig. \ref{landau}); Landau-Niveaus, die sich unterhalb der Fermienergie befinden, sind bei \(T=0\) komplett befüllt, während solche, die sich bei höheren Energien befinden, komplett leer sind. Da die Landauniveaus proportional zu \(B\) sind, sinkt die Anzahl der besetzten Niveaus mit steigendem Magnetfeld. Dies wird beschrieben durch den Füllfaktor
\begin{equation}
\label{nu}
\nu = \frac{n_e h}{e B}.
\end{equation}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{landau.png}
	\caption{Die Zustandsdichte eines 2DEG im Magnetfeld}
	\label{landau}
\end{figure}

Es muss erwähnt werden, dass die Zustandsdichte aufgrund von Defekten nicht eine Summe von Delta-Peaks ist, sondern dass diese Peaks wesentlich verbreitert sind, wodurch eventuell die zu den beiden Spin-Ausrichtungen gehöhrenden Peaks nicht mehr aufgelöst werden können. Die Modulation der Energieverteilung aufgrund der Defekte führt in vollbesetzten Landau-Niveaus zu einer starken Lokalisierung.

\subsection{Randkanäle}
In der Realität hat die Probe eine endliche Ausdehnung, was zu einem sehr hohen Potential an den Rändern der Probe führt. Dies verhindert dort eine Lokalisierung der Elektronen; stattdessen werden die Elektronen am Rand "`reflektiert"', was zu widerstandsfreien Randkanälen führt. Die Anzahl dieser Kanäle entspricht der Zahl der Landauniveaus, deren Energien unterhalb der Fermienergie liegen. Der Querwiderstand pro Randkanal beträgt
\begin{equation}
R_{\text{xy}} = \frac{h}{e^2}.
\end{equation}
Falls die Fermienergie genau innerhalb eines Landau-Niveaus liegt, so können die dort enthaltenen Zustände thermisch streuen. Dies hebt die Lokalisierung auf und das gesamte 2DEG trägt zum Stromtransport bei. Paradoxerweise ist in diesem Fall der Längswiderstand jedoch höher als wenn \(E_f \neq E_{\text{Landau}}\), da die Streuung auch mit den Randkanälen stattfindet, was dazu führt, dass sie nicht mehr widerstandsfrei sind.\\
Dies führt zu den \textbf{Shubnikov-de-Haas-Oszillationen}: Der Längswiderstand fällt immer dann ab, wenn das Ferminiveau kein Landauniveau trifft, da dann die widerstandsfreien Randkanäle den Strom tragen. Liegt die Fermienergie in einem Landau-Niveau, so findet Streuung auch in den Randkanälen statt und der Längswiderstand steigt an.

Die Maxima des Längswiderstandes treten bei halbzahligem Füllfaktor auf; dadurch kann man die Ladungsträgerdichte durch Kenntnis der Magnetfelder zweier aufeinanderfolgender Maxima der Längswiderstände berechnen:
\begin{equation}
\label{minmax}
n_e = \frac{e}{h} \frac{1}{\frac{1}{B_i}-\frac{1}{B_{i+1}}}.
\end{equation}
\subsection{Quanten-Hall-Effekt}

\section{Versuch}
\subsection{Aufbau}
In diesem Versuch wurde eine GaAs/AlGaAs-Heterostruktur in einem Vakuumrohr, das mit flüssigem Helium gekühlt wurde, als Probe verwendet. Die Probe bestand aus zwei Teilen: Eine war unstrukturiert, die andere wies ein Übergitter aus runden Antidots auf (Gitterkonstante \(750\,nm\)). Die Probe wurde an eine Spannungsquelle angeschlossen, um einen Strom fließen zu lassen. Da jedoch keine Konstantstromquelle zur Verfügung stand, wurde ein Vorwiderstand von \(\SI{1}{\mega\ohm}\) in Serie geschaltet und dann die Spannung auf \(\SI{2}{\volt})\) eingestellt, damit -- unter Vernachlässigung des Probenwiderstandes -- ein Strom von \(\SI{2}{\micro\ampere}\) floss. Kontakte an den Seiten der Probe ermöglichten es, Spanungsabfälle parallel sowie senkrecht zur Stromrichtung in beiden Bereichen zu messen (Verstärkung mit Differenzverstärker um den Faktor 100). Mit Hilfe einer Magnetspule aus supraleitendem NbTi-Draht konnte ein Magnetfeld senkrecht zur Probe erzeugt werden (Spulenstrom \SIrange{0}{25}{\ampere}, Spulenkonstante \(\SI{0.226}{\tesla\per\ampere}\)). Eine Gatterspannung konnte angelegt werden, um den Potentialverlauf in der Probe und damit die Ladungsträgerdichte variieren zu können.

Die Kontakte zur Spannungsmessung an der Probe sowie Kontake des Magnetnetzgerätes wurden mit einem XY-Schreiber verbunden, der bei Veränderung des Magnetfeldes die dazugehörige Messspannung aufzeichnen konnte.

\subsection{Durchführung}
Es wurde für beide Bereiche der Probe sowohl der Spannungsabfall in Längsrichtung, als auch in Querrichtung für ein durchfahrendes Magnetfeld gemessen, wobei jeweils 3 verschiedene Gate-Spannungen angelegt wurden: \(U_g \in \{-0.15, 0, 0.5\}V\)
\subsection{Auswertung}
Da kein ausreichend großer Scanner zur Verfügung stand, wurden die Messsergebnisse zweiseitig eingescannt und dann mit Hilfe eines Bildbearbeitungsprogramms zusammengefügt:
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{S.jpg}
    \caption{Die Ergebnisse für die Messung im strukturierten Bereich: Die x-Achse ist proportional zum Magnetfeld, die y-Achse zur Messspannung. Die schwarze (\(\SI{0.2}{\volt\per\centi\metre}\)), orangene (\(\SI{0.2}{\volt\per\centi\metre}\)) und pinke (\(\SI{0.1}{\volt\per\centi\metre}\)) Kurve ist die Querspannung und die rote (\(\SI{0.1}{\volt\per\centi\metre}\)), violette (\(\SI{0.5}{\milli\volt\per\centi\metre}\)) und blaue (\(\SI{5}{\milli\volt\per\centi\metre}\)) die Längsspannung mit der jeweiligen Gate-Spannung \(\SI{-0.15}{\volt}\), \(\SI{0}{\volt}\) und \(\SI{0.5}{\volt}\). Die x-Empfindlichkeit betrug \(\SI{2}{\milli\volt}/\SI{}{\centi\metre}\). Die rote Kurve gehört zu einer fehlerhaften Messung und wurde ignoriert.}
    \label{spektrum_174}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{US.jpg}
    \caption{Die Ergebnisse für die Messung im unstrukturierten Bereich: Die x-Achse ist proportional zum Magnetfeld, die y-Achse zur Messspannung. Die blaue (\(\SI{0.2}{\volt\per\centi\metre}\)), schwarze (\(\SI{0.1}{\volt\per\centi\metre}\)) und grüne (\(\SI{0.1}{\volt\per\centi\metre}\)) Kurve ist die Querspannung und die rote (\(\SI{0.01}{\volt\per\centi\metre}\)), violette (\(\SI{5}{\milli\volt\per\centi\metre}\)) und pinke (\(\SI{5}{\milli\volt\per\centi\metre}\)) die Längsspannung mit der jeweiligen Gate-Spannung \(\SI{-0.15}{\volt}\), \(\SI{0}{\volt}\) und \(\SI{0.5}{\volt}\). Die x-Empfindlichkeit betrug \(\SI{2}{\milli\volt}/\SI{}{\centi\metre}\). Die untere violette und rote Kurve gehören zu fehlerhaften Messungen und wurden ignoriert.}
	\label{spektrum_174}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{detail.jpg}
	\caption{Detailaufnahme der Längsspannung im strukturierten Bereich: Grün: \(\SI{5}{\milli\volt\per\centi\metre}, V_g=\SI{-0.15}{\volt}\); Schwarz: \(\SI{2}{\milli\volt\per\centi\metre}, V_g=\SI{0}{\volt}\); Blau: \(\SI{5}{\milli\volt\per\centi\metre}, V_g=\SI{0.5}{\volt}\).  Die x-Empfindlichkeit betrug \(\SI{0.5}{\milli\volt}/\SI{}{\centi\metre}\).}
	\label{spektrum_174}
\end{figure}

Da die Messwerte aufgrund der verschiedenen Verstärkungsfaktoren der einzelnen Kurven nicht gut vergleichbar sind, wurde das Programm PlotDigitizer verwendet, um die Daten zu digitalisieren. Mit Hilfe der Verstärkungsfaktoren und der Spulenkonstante \(\SI{0.226}{\tesla\per\ampere}\) konnten die Daten in die Einheiten Volt und Tesla umgerechnet werden. Diese digitalisierten Daten wurden jedoch nur zur Visualisierung verwendet; da die Digitalisierung eine weitere mögliche Ursache für Messfehler darstellt, wurden genaue Messungen immer direkt auf dem Millimeterpapier vorgenommen.

Im Folgenden wird folgendes Benennungsschema für die einzelnen Datensätze verwendet: Eine Zahl gibt die Gatespannung an, der Buchstabe \textbf{S} steht für eine Messung im strukturierten Bereich, während \textbf{US} für eine Messung im unstrukturierten Bereoich steht. \textbf{-0.15S} ist also der Bezeichner von \(V_g=\SI{-0.15}{\volt}\) im strukturierten Bereich.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{quer.png}
	\caption{Die Abhängigkeit der Querspannung vom Magnetfeld. Gestrichelte Kurven sind Messungen im unstrukturierten Bereich, durchgezogene Linien im strukturierten Bereich.}
	\label{quer}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{laengs.png}
	\caption{Die Abhängigkeit der Längsspannung vom Magnetfeld. Gestrichelte Kurven sind Messungen im unstrukturierten Bereich, durchgezogene Linien im strukturierten Bereich.}
	\label{laengs}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{laengs-detail.png}
	\caption{Detailmessung der Längsspannung im strukturierten Bereich.}
	\label{detail}
\end{figure}

\subsubsection{Fehlerabschätzung}
Folgende Fehlerquellen können auftreten:
\begin{itemize}
	\item \textbf{Stromveränderung}: Es wurde angenommen, dass der Widerstand der Probe im Vergleich zum Vorwiderstand von \(\SI{1}{\mega\ohm}\) vernachlässigbar ist, und dass damit der Strom durch die Probe konstant ist, unabhängig vom anliegenden Magnetfeld. Es wurde jedoch gemessen, dass bei vollständigem Durchfahren des Magnetfeldes die Spannung, die am Vorwiderstand anliegt, von \(\SI{2}{\volt}\) auf \(\SI{1.967}{\volt}\) abfällt. Dies bedeutet, dass der Widerstand der Probe sich geändert hat:
	\begin{align*}
		R_{\text{probe}}& = R_{\text{res}} \cdot \frac{U_{\text{probe}}}{U_{\text{res}}},\\
		R_{\text{ges}} &= R_{\text{res}} + R_{\text{probe}},\\
		I &= \frac{U_{\text{ges}}}{R_{\text{ges}}}\\
		  &= \frac{\SI{2}{\volt}}{\SI{1}{\mega\ohm} \left( 1 + \frac{\SI{0.33}{\volt}}{\SI{1.967}{\volt}} \right)}\\
		  &= \SI{1.71}{\micro\ampere}
	\end{align*}
	Dieser Wert für den Strom bei vollem Magnetfeld ist signifikant kleiner als der Ursprungswert von \(\SI{2}{\micro\ampere}\). Deswegen wurde bei der Auswertung \(I=\SI{1.85\pm 0.15}{\micro\ampere}\) verwendet.
	\item \textbf{Ablesegenauigkeit}: Die Ablesegenauigkeit auf dem Millimeterpapier beträgt aufgrund der verwendeten Strichbreite etwa \(\SI{1}{\milli\metre}.\)
	\item \textbf{Ungenauigkeit des X-Y-Schreibers:} Die Aufzeichnungsgenauigkeit des X-Y-Schreibers wurde auf \(\SI{1}{\milli\meter}\) geschätzt.
\end{itemize}

\subsubsection{Ermittlung der Ladungsträgerdichte mittels Betrachtung der Hallspannung}
Die in Abb. \ref{quer} abgebildeten Kurven der Hallspannung entsprechen den Erwartungen: Bei niedrigen Magnetfeldern existiert ein linearer Zusammenhang zwischen Hall-Spannung und Magnetfeld, genau wie es klassisch erwartet wird. Bei hohen Magnetfeldern tritt der Quanten-Hall-Effekt auf, der zu den gemessenen Plateaus führt.
Wie im Theorieteil beschrieben, ist die Ladungsträgerdichte bei geringen Werten für \(B\) indirekt proportional zur Ableitung der Hallspannung nach dem Magnetfeld (siehe Eq. \ref{ne_abl}). Es wurde deswegen an die Daten aus Fig. \ref{quer} für Werte mit \(B < \SI{1.5}{\tesla}\) eine Ursprungsgerade gefittet. Die so erhaltenen Steigungen wurden mittels Eq. \ref{ne_abl} in Ladungsträgerdichten umgerechnet. Der Fehler wurde mit Hilfe Gauß'scher Fehlerfortpflanzung errechnet, wobei die Ungenauigkeit des Stromwertes sowie die Unsicherheit des Fits mit einbezogen wurden:

\input{ne}



\subsubsection{Füllfaktoren}
Nun wurden die Plateaus des Hallwiderstands betrachtet: Es wurde sowohl der Magnetfeldwert als auch die korrespondierende Hallspannung aus den Plots gemessen. Aufgrund von Eq. \ref{foo} müssen die Hall-Spannungen folgende Gleichung erfüllen:
\begin{equation}
	U_\nu = \frac{U_{\text{max}}}{\nu}.
\end{equation}
Die Füllfaktoren für die einzelnen Plateaus wurden nun ermittelt, indem überprüft wurde, ob die obenstehende Gleichung erfüllt ist.


\begin{itemize}
\input{plateau}
\end{itemize}

Um zu überprüfen, ob die erhaltenen Füllfaktoren realistisch sind, wurde Eq. \ref{foo} unter Verwendung der im vorherigen Abschnitt ermittelten Ladungsträgerdichten geplottet, und anschließend die in diesem Versuch ermittelten Füllfaktorpunkte in den gleichen Graph eingezeichnet (Fig. \ref{fuellfaktor}). DAS GEHT SICH SO SEMI-GUT AUS, ABER: UM DIE LINIEN ZU ZEICHNEN WURDE N_E AUS DEM VORIGEN VERSUCH VERWENDET, DAS UNGENAUIGKEIT HAT, DIE NICHT EINGEZEICHNET WURDE

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{fuellfaktor.png}
	\caption{Durchgezogene Linien und Punkte stehen für den strukturierten Bereich, während gestrichtelte Linien und Sterne für den unstrukturierten Bereich stehen.}
	\label{fuellfaktor}
\end{figure}

\subsubsection{Ladungsträgerdichte aus SdH-Oszillationen}

Die Position der Maxima und Minima der Längsspannung wurden aus den Daten gemessen und in die Einheit Tesla umgerechnet. Mit Hilfe der Ergebnisse des vorigen Abschnitts wurden ihnen dann die entsprechenden Füllfaktoren zugewiesen.


\begin{itemize}
\input{sdh}
\end{itemize}

Anschließend wurden die entsprechenden Füllfaktoren als Funktion von \(1/B\) aufgetragen (Fig. \ref{ne_fuell}). Aufgrund von Eq. \ref{nu} wurden Ursprungsgeraden erwartet, was sich gut bestätigte. Es wurde nun jeweils für alle aufeinanderfolgenden Minima / Maxima mit Hilfe von Eq. \ref{minmax} die Ladungsträgerdichte berechnet und schließlich der Mittelwert gebildet: (ALTERNATIV: MAN KÖNNTE EINE GERADE FITTEN)

\begin{table}[H]
\begin{tabular}{ll}
\textbf{Name}	&  \textbf{Ladungsträgerdichte}  \\
\input{ne_fuell}
\end{tabular}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{n_e.png}
	\caption{Durchgezogene Linien und Punkte stehen für den strukturierten Bereich, während gestrichtelte Linien und Sterne für den unstrukturierten Bereich stehen.}
	\label{ne_fuell}
\end{figure}

\subsubsection{Ermittlung der Beweglichkeit}
Zur Ermittlung der Beweglichkeit der Elektronen wurde die Längsspannung bei \(B=0\) abgelesen und in Spannungen umgerechnet. Anschließend konnte mittels Eq. \ref{beweglichkeit} die Beweglichkeit der Elektronen berechnet werden.
\input{beweglichkeit}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{beweglichkeit.png}
	\caption{Werte für den unstrukturierten Bereich sind in rot, Werte für den strukturierten Bereich in blau.}
	\label{beweglichkeit}
\end{figure}

\subsubsection{Vergleich der Werte für die Ladungsträgerdichte}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{vergleich_ne.png}
	\caption{Werte für den unstrukturierten Bereich sind in rot, Werte für den strukturierten Bereich in blau. Punkte symbolisieren Werte, die mittels der ersten Methode ermittelt wurden, Sterne solche, die mit der zweiten Methode gefunden wurden.}
	\label{ne_vergleich}
\end{figure}

\section{Detailuntersuchung des strukturierten Bereichs}

Wie im Theorieteil erläutert wird erwartet, dass Runaway-Trajektorien dominieren, wenn der Radius der Zyklotronbahnen kleiner als die Hälfte des Gitterabstandes \(a=\SI{750}{\nano\metre}\) ist. Mit Eq. \ref{radius} ergibt sich für das kritische Magnetfeld:
\begin{equation}
B_{\text{krit}} = \frac{\hbar}{e\cdot 0.5\cdot a} \sqrt{2\pi n_e}.
\end{equation}

\newpage
Unter Verwendung der in den vorherigen Abschnitten ermittelten Ladungsträgerdichten ergibt sich:
\input{crit_magn}
Es wurde erwartet, dass die gemessene Längsspannung in Fig. \ref{detail} ab diesem Wert stark abfällt, jedoch ist dies nicht so. WARUM IST DAS SO???

Interpretationsversuch der Maxima in Fig. \ref{detail}: Alle drei Kurven haben 3 (mehr oder weniger) 2 Peaks: Der Linke davon steht für Lokalisierung, bei der die Elektronen 4 ANtidots umkreisen, bei dem rechten davon umkreisen sie nur noch einen, danach ists dann ein runaway. Die Frage ist halt, warum runaway erst bei relativ hohen Magnetfeldern auftritt
\end{document}
